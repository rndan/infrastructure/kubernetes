from jinja2 import Environment, FileSystemLoader
import sys

machines_num = int(sys.argv[1])
machines_arch = sys.argv[2]
broker_ip = sys.argv[3]

environment = Environment(loader=FileSystemLoader("./"))
template = environment.get_template("rndan-app.jinja2")

for num in range(1, machines_num + 1):
    filename = f"rndan-app-{num}.yml"
    content = template.render(
        name = f"rndan-app-{num}",
        devid = num,
        arch = machines_arch,
        broker_ip = broker_ip
    )
    with open(filename, mode="w", encoding="utf-8") as manifest:
        manifest.write(content)
        print(f"{filename} created")
